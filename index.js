var request = require('request');

/*
    Options:
        token - required
        tags - optional
*/
function Logger(options) {
    if (!(this instanceof Logger)) {
        return new Logger(options);
    }
    if (!options || typeof options.token !== 'string') {
        throw new Error('Expected a customer token!');
    }
    this.url = 'http://logs-01.loggly.com/inputs/' + options.token + '/';
    this.tags = options.tags || [];
};

/*
    Usage:
        log(body[, tags][, callback])
*/
Logger.prototype.log = function(body, tags, callback) {
    var url = this.url;
    if (typeof tags === 'function') {
        callback = tags;
        tags = null;
        if (this.tags.length > 0) {
            url += 'tag/' + this.tags.join();
        }
    } else {
        url += 'tag/' + this.tags.concat(tags).join();
    }
    request.post(url, {json: body}, callback);
};

module.exports = Logger;
