# Loggerly
A super simple module to log events to Loggly using the Loggly API.

## Usage
Simple example:

```javascript
var Logger = require('loggerly');

var logger = new Logger({
	token: 'your-customer-token'
});

logger.log('Hello world');
```

## Options
The constructor takes an options argument. The possible options are:

- token - your Loggly customer token
- tags *(optional)* - an array of tags to add to all events

## Methods
Methods available on the logger.

### logger.log(data[, tags][, callback])

Parameters:

- data - the event data to be sent as JSON
- tags *(optional)* - an array of tags to add to this event
- callback *(optional)* - the callback to call when this event has been logged